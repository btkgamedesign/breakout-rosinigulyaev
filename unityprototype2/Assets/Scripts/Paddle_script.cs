﻿using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;

public class Paddle_script : MonoBehaviour
{
    public Settings settings;
    public Rigidbody puck;
    public Joint joint;
    public List<Rigidbody> extrapucks;
    public float Rightlimit;
    public float Leftlimit;
    public int combocounter = 0;

    void Start()
    {
        DOTween.Init();
        DOVirtual.DelayedCall(1, NewPuck);
    }
    void Update()
    {
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Translate(transform.right * settings.speedFactor * Time.deltaTime);

        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Translate(transform.right * -settings.speedFactor * Time.deltaTime);

        }

        float newX = Mathf.Clamp(transform.position.x, Leftlimit, Rightlimit);
        if (transform.position.x != newX)
        {
            transform.position = new Vector3(newX, transform.position.y, transform.position.z);
        }

        if (Input.GetKeyUp(KeyCode.Space) && joint != null)
        {
            joint.breakForce = 0;

            puck.AddForce(new Vector3(0, 1, 0));
            puck.GetComponent<Puck_script>().launched = true;
        }
    }
    void OnJointBreak(float breakForce)
    {
        puck.AddForce(new Vector3(0, settings.speedFactor, 0), ForceMode.VelocityChange);
        puck.GetComponent<Puck_script>().launched = true;
    }
    public void NewPuck()
    {
        if (extrapucks.Count == 0)
        {
            StateManager_script.Instance.Restart();
        }
        else
        {
            puck = extrapucks[0];
            extrapucks.RemoveAt(0);

            puck.transform.position = transform.position + Vector3.up * 0.5f;
            joint = gameObject.AddComponent<FixedJoint>();
            joint.connectedBody = puck;
        }
    }
    void OnCollisionEnter(Collision collision)
    {
        float xDiff = puck.transform.position.x - transform.position.x;
        combocounter = 0;
        Vector3 direction = new Vector3(xDiff * settings.speedFactor, 0, 0);
        puck.AddForce(direction, ForceMode.VelocityChange);
        GetComponent<AudioSource>().Play();
    }
}
