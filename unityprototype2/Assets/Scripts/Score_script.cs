﻿
using UnityEngine;
using UnityEngine.UI;

public class Score_script : MonoBehaviour
{
    public static Score_script Instance;
    Text label;
    int score = 0;

    // Start is called before the first frame update
    public void Start()
    {
        Instance = this;
        label = GetComponent<Text>();
    }

    // Update is called once per frame
    void UpdateLabel()
    {
        label.text = $"Score: <color=yellow>{score}</color>";
    }
    public void ScoreAdd(int toAdd)
    {
        score = score + toAdd;
        UpdateLabel();
    }
}
