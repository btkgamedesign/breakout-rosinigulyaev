﻿using DG.Tweening;
using UnityEngine;

public class Puck_script : MonoBehaviour
{
    public Settings settings;
    public Rigidbody rb;
    public bool launched;
    const float minSpeed = 7.5f;
    const float maxSpeed = 15f;
    public Material material;
    ParticleSystem Particle;
    ParticleSystem.MainModule ParticleMain;
    Renderer Rend;
    Tweener punchTween;
    private void Awake()
    {
        DOTween.Init();
        Particle = GetComponentInChildren<ParticleSystem>();
        ParticleMain = Particle.main;
        Rend = GetComponentsInChildren<Renderer>()[1];
    }
    void FixedUpdate()
    {

        if (launched)
        {
            float minSpeed = settings.speedFactor / 1.2f;
            float maxSpeed = settings.speedFactor * 1.2f;
            float speed = rb.velocity.magnitude;
            if (speed < minSpeed)
            {
                rb.velocity *= settings.speedFactor / minSpeed;
            }
            else if (speed > maxSpeed)
            {
                rb.velocity *= settings.speedFactor / maxSpeed;
            }
        }
    }
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Brick")
        {
            material.DOColor(Color.white, 0.15f);
            DOVirtual.DelayedCall(0.1f, ColorReturn);
            ParticleMain.startColor = new Color(255, 255, 255, 100);
            Particle.Play();
            if (punchTween != null)
            {
                punchTween.Kill();
                Rend.transform.localScale = Vector3.one;
            }

            punchTween = Rend.transform.DOPunchScale(Vector3.one / 2f, .15f);
        }
        else
        {
            ParticleMain.startColor = new Color(0, 0, 0, 100);
            Particle.Play();
            if (punchTween != null)
            {
                punchTween.Kill();
                Rend.transform.localScale = Vector3.one;
            }

            punchTween = Rend.transform.DOPunchScale(Vector3.one / 2f, .15f);
        }

    }
    void ColorReturn()
    {
        material.DOColor(Color.black, 0.1f);
    }
}
