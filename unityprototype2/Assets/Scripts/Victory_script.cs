﻿using DG.Tweening;
using UnityEngine;

public class Victory_script : MonoBehaviour
{
    public GameObject victoryobj;
    public SpriteRenderer material;
    public CameraEffect_script cameraeffect;
    public AudioSource yay;
    void OnCollisionEnter(Collision collision)
    {
        collision.rigidbody.AddForce(Vector3.up * 5, ForceMode.Impulse);
        cameraeffect.Shake();
        material.DOColor(Color.yellow, 0.15f);
        victoryobj.transform.DOScale(2, 0.1f);
        Score_script.Instance.ScoreAdd(10);
        DOVirtual.DelayedCall(0.1f, ColorReturn);
        yay.Play();
    }
    void ColorReturn()
    {
        victoryobj.transform.DOScale(1, 0.1f);
        material.DOColor(Color.white, 0.1f);
    }
}
