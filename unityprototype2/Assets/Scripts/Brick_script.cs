﻿using DG.Tweening;
using UnityEngine;

public class Brick_script : MonoBehaviour
{
    public static Vector3 hit;
    public static int Bricks;
    public SpriteRenderer Victory;
    public AudioSource Yaaaaaay;
    public Paddle_script paddle;
    public CameraEffect_script cameraeffect;
    public AudioSource Audio1;
    public AudioSource Audio2;
    public AudioSource Audio3;
    public AudioSource Audio4;
    public AudioSource Audio5;
    public AudioSource Audio6;
    public AudioSource Audio7;
    public AudioSource Audio8;
    public GameObject TrophyCollider;
    private void Start()
    {
        Bricks++;
    }

    void OnCollisionEnter(Collision collision)
    {
        cameraeffect.Shake();
        enabled = false;
        GetComponent<Renderer>().enabled = false;
        GetComponent<Collider>().enabled = false;
        Bricks--;
        if (Bricks == 0)
        {
            Victory.enabled = true;
            TrophyCollider.SetActive(true);
            DOVirtual.DelayedCall(10, StateManager_script.Instance.Restart);
            Yaaaaaay.Play();
        }

        if (paddle.combocounter == 0)
        {
            Audio1.Play();
            paddle.combocounter++;
            Score_script.Instance.ScoreAdd(100);
        }
        else if (paddle.combocounter == 1)
        {
            Audio2.Play();
            paddle.combocounter++;
            Score_script.Instance.ScoreAdd(110);
        }
        else if (paddle.combocounter == 2)
        {
            Audio3.Play();
            paddle.combocounter++;
            Score_script.Instance.ScoreAdd(120);
        }
        else if (paddle.combocounter == 3)
        {
            Audio4.Play();
            paddle.combocounter++;
            Score_script.Instance.ScoreAdd(130);
        }
        else if (paddle.combocounter == 4)
        {
            Audio5.Play();
            paddle.combocounter++;
            Score_script.Instance.ScoreAdd(140);
        }
        else if (paddle.combocounter == 5)
        {
            Audio6.Play();
            paddle.combocounter++;
            Score_script.Instance.ScoreAdd(150);
        }
        else if (paddle.combocounter == 6)
        {
            Audio7.Play();
            paddle.combocounter++;
            Score_script.Instance.ScoreAdd(200);
        }
        else if (paddle.combocounter >= 7)
        {
            Audio8.Play();
            paddle.combocounter++;
            Score_script.Instance.ScoreAdd(300);
        }
    }

}
