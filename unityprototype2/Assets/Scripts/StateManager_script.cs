﻿using UnityEngine;
using UnityEngine.SceneManagement;
public class StateManager_script : MonoBehaviour
{
    public static StateManager_script Instance;
    void Start()
    {
        Instance = this;
    }
    public void Restart()
    {
        SceneManager.LoadScene("Main");
    }

}
