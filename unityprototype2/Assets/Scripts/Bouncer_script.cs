﻿using DG.Tweening;
using UnityEngine;
public class Bouncer_script : MonoBehaviour
{
    public Material material;
    public CameraEffect_script cameraeffect;
    void OnCollisionEnter(Collision collision)
    {
        collision.rigidbody.AddForce(Vector3.up * 5, ForceMode.Impulse);
        cameraeffect.Shake();
        material.DOColor(Color.white, 0.15f);
        DOVirtual.DelayedCall(0.1f, ColorReturn);

        //DOVirtual.DelayedCall(0.1f,

        //() => transform.parent.DOsca....
        //);
    }
    void ColorReturn()
    {
        material.DOColor(Color.black, 0.1f);
    }
}
